#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_sargo.mk

COMMON_LUNCH_CHOICES := \
    lineage_sargo-user \
    lineage_sargo-userdebug \
    lineage_sargo-eng
