#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_SHIPPING_API_LEVEL := 28

PRODUCT_CHARACTERISTICS := nosdcard

PRODUCT_BUILD_SUPER_PARTITION := false
PRODUCT_USE_DYNAMIC_PARTITIONS := true

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Enable updating of APEXes
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)

# Include GSI keys
$(call inherit-product, $(SRC_TARGET_DIR)/product/gsi_keys.mk)

# A/B
PRODUCT_PACKAGES += \
    android.hardware.boot@1.2-impl \
    android.hardware.boot@1.2-impl.recovery \
    android.hardware.boot@1.2-service

PRODUCT_PACKAGES += \
    update_engine \
    update_engine_sideload \
    update_verifier

AB_OTA_POSTINSTALL_CONFIG += \
    RUN_POSTINSTALL_system=true \
    POSTINSTALL_PATH_system=system/bin/otapreopt_script \
    FILESYSTEM_TYPE_system=ext4 \
    POSTINSTALL_OPTIONAL_system=true

AB_OTA_POSTINSTALL_CONFIG += \
    RUN_POSTINSTALL_vendor=true \
    POSTINSTALL_PATH_vendor=bin/checkpoint_gc \
    FILESYSTEM_TYPE_vendor=ext4 \
    POSTINSTALL_OPTIONAL_vendor=true

PRODUCT_PACKAGES += \
    checkpoint_gc \
    otapreopt_script

# fastbootd
PRODUCT_PACKAGES += \
    android.hardware.fastboot@1.1-impl-mock \
    fastbootd

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-service

# Rootdir
PRODUCT_PACKAGES += \
    init.edge_sense.sh \
    init.fingerprint.sh \
    init.firstboot.sh \
    init.insmod.sh \
    init.qcom.devstart.sh \
    init.qcom.ipastart.sh \
    init.qcom.wlan.sh \
    init.radio.sh \
    init.ramoops.sh \

PRODUCT_PACKAGES += \
    fstab.sdm670 \
    init.bonito.rc \
    init.sargo.rc \
    init.sdm670.diag.rc \
    init.sdm670.logging.rc \
    init.sdm670.mpssrfs.rc \
    init.sdm670.power.rc \
    init.sdm670.rc \
    init.sdm670.usb.rc \
    init.recovery.sargo.rc \
    init.recovery.bonito.rc \
    init.recovery.sdm670.rc \

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/rootdir/etc/fstab.sdm670:$(TARGET_COPY_OUT_RAMDISK)/fstab.sdm670

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/google/sargo/sargo-vendor.mk)
